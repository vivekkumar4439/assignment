from amazoncaptcha import amazoncaptcha
*** Settings ***
Library    SeleniumLibrary
Resource    ../Resources/Variables.robot
Resource    ../Resources/CommonFunctionality.robot
Resource    ../Resources/Keywords.robot
Test Setup    Start Testcase    ${url}
Test Teardown    Finish Testcase

*** Test Cases ***
TC1
    Selecting Product and Filters
    Title Verification
    Price Verification
    Purchase
    Sign-in Verification
    