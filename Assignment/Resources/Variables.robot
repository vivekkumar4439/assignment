*** Variables ***
${url}    https://www.amazon.in/
${prodCategory}    //a[text()='Mobiles']
${prodSubCategory}    //a[contains(@aria-label,'Mobiles')]
${Dropdownitem}    //a[text()='Headsets']
${Primeicon}    i[aria-label='Prime Eligible']
${inputBox}    input#twotabsearchtextbox
${product}    Headphones
${searchButton}    nav-search-submit-button
${sortButton}    a-autoid-0-announce
${lowTohigh}    a#s-result-sort-select_1
${titleCSS}    span.a-size-base-plus.a-color-base.a-text-normal
${expected_title}    TIGERIFY Extra Bass Earphones Headphones Headset 3.5mm Jack with Mic (White)
${priceCSS}    span.a-price-whole
${expected_price}    118
${product_Icon}    //span[text()='${expected_title}']
${buynow_Button}    input#buy-now-button
