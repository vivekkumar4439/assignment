*** Settings ***
Library    SeleniumLibrary
Resource    Variables.robot

*** Keywords ***
Selecting Product and Filters
    Click Element    xpath:${prodCategory}
    Mouse Over    xpath:${prodSubCategory}
    Click Element    xpath:${Dropdownitem}
    Scroll Element Into View    css:${Primeicon}
    Click Element   css:${Primeicon}
    Input Text    css:${inputBox}    ${product}
    Click Element    id:${searchButton}
    Click Element    id:${sortButton}
    Click Element    css:${lowTohigh}
    Sleep    2s

Title Verification
    ${elements}=    Get WebElements    css:${titleCSS}
    ${actual_title}=    Get Text    ${elements[0]}
    Log    ${actual_title}
    Should Be Equal As Strings    ${actual_title}    ${expected_title}

Price Verification
    ${elements}=    Get WebElements    css:${priceCSS}
    ${actual_price}=    Get Text    ${elements[3]}
    Log    ${actual_price}
    Should Be Equal As Strings    ${actual_price}    ${expected_price}

Purchase
    Click Element    xpath:${product_Icon}
    Switch Window    NEW
    Click Element    css:${buynow_Button}     
    
Sign-in Verification
    Page Should Contain    Sign in 
