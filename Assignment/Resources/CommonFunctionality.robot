*** Settings ***
Library    SeleniumLibrary

*** Keywords ***
Start Testcase
    [Arguments]    ${url}
    Open Browser    ${url}    chrome
    Maximize Browser Window

Finish Testcase    
    Close All Browsers